let tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false }
  ];

let dishes = {title: "Do the dishes", isComplete: true}


// Ajouter tâche
// Fonction fléchée
// Spread opérateur : rappelle les éléments du tableau qui sera mis en paramètre
  let addTask = (tasksList, task) => tasks = [...tasksList, task];
 addTask(tasks, dishes);



// Retirer une tâche spécifique
// Fonction fléchée
// Méthode .filter : ne conserve que les éléments 'vrais', ici toutes les tâches dont le titre est différent de 'Do the dishes'
  let removeTasks = (taskRemove) => { tasks = tasks.filter(task => task.title !== `${taskRemove}`)};
  removeTasks('Do the dishes');


  // Retirer toutes les tâches .isCompleted = true
  let removeCompletedTasks = (listTasks) =>  listTasks.filter(task => task.isComplete ? false : true )

  // Function fléchée version simplifiée
  // let removeCompletedTasks = (listTasks) =>  listTasks.filter((task) => !task.isComplete

// Basculer l'état d'une tâche
// Attribution dans d'un booléen contraire à celui existant pour chaque index du tableau, à la propriété .isComplete.
const toggleTaskStatus = (taskIndexToToggle) => {
    tasks[taskIndexToToggle].isComplete = tasks[taskIndexToToggle].isComplete == true ? false : true;
}
toggleTaskStatus(0);
toggleTaskStatus(1);
//console.log(tasks)  

// Afficher toutes les tâches
const allTasks = () => console.log(tasks);

//Afficher que les tâches complétées (.map)
const tasksTodoMap = tasks.map((task) => {task.isComplete == false})
//console.log(tasksTodoMap);



// Afficher que les tâches incomplètes (.filter)
// Afficher que les titres des tâches complètes (.map)
const stillToDo = () => {
    let tasksToDo = tasks.filter(task => task.isComplete == false);
    // for (eachTask in tasksToDo) {
    //     console.log("Les tâches restantes à faire sont les suivantes :"+ tasksToDo[eachTask].title)
    // }
    console.log("Liste des tâches restantes à faire (seulement leurs titres) : ");
    console.log(tasksToDo.map(task => task.title));
}
// stillToDo()


// Afficher que les tâches complètes (.filter)
// Afficher que les titres des tâches complètes (.map)
const alreadyDone = () => {
    let tasksDone = tasks.filter(task => task.isComplete == true);
    console.log("Les tâches effectuées sont les suivantes")
    console.log( tasksDone.map(task => task.title));
}
//alreadyDone()
